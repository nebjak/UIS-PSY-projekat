class UserMethods {
    public CommandResponse addUser(User u) {}
    public User[] getUser(int user_id = null) {}
    public CommandResponse updateUser(User u) {}
    public CommandResponse deleteUser(int user_id) {}

    public User authenticateUser(String username, String password) {}
}
